#!/usr/bin/env python

import re, sys
import urllib
import os, subprocess
import getopt
from xml.dom.minidom import parseString
from warnings import warn
from optparse import OptionParser

def fetch_recentchanges(namespace, fromdate=None):
    """
    This looks at the recent changes and downloads only changes since the last commit.
    """
    global git_path
    if fromdate is None:
        history = subprocess.Popen([git_path, "rev-list", 'HEAD', '--format=tformat:%at', '-n 1'], stdout=subprocess.PIPE).communicate()[0]
        fromdate = history.split("\n")[1]
    # http://www.amateur-radio-wiki.net/api.php?action=query&list=recentchanges&rclimit=50&rcprop=title|timestamp|ids|loginfo
    params = {'action': 'query', 'list': 'recentchanges', 'rcstart': fromdate, 'rcdir': 'newer', 'rcprop': 'title|timestamp|ids|loginfo', 'rclimit': options.limit, 'format': 'xml'}
    if namespace is not None:
        params['rcnamespace'] = "|".join(map(lambda i: str(i), namespace))
    params = urllib.urlencode(params)
    url = "%s/api.php?%s" % (options.base_url, params)

    if options.verbose:
        print "getting changes from %s to now from %s" % (fromdate, url)
    dom = parseString(urllib.urlopen(url).read())
    changes = dom.getElementsByTagName('rc')
    cont = dom.getElementsByTagName('query-continue')

    # XXX: this should be optimised out of recentchanges
    info = namespace_info()

    for change in changes:
        if info[int(change.getAttribute('ns'))] == 'File':
            filename = fetch_file(change.getAttribute('title'), change.getAttribute('revid'))
        else:
            filename = fetch_page(change.getAttribute('title'), change.getAttribute('revid'))
        if filename:
            # finally, add everything to git
            if not os.path.exists(".git"):
                subprocess.check_call([git_path, "init"])
            subprocess.check_call([git_path, "add", filename])
            comment = change.getAttribute('comment')
            if comment == '':
                comment = "automatic import from MediaWiki page %s at url %s" % (change.getAttribute('title'), options.base_url)
            subprocess.check_call([git_path, "commit", "-m", comment])

    if len(cont) > 0:
        fetch_recentchanges(namespace_id, cont[0].firstChild.getAttribute('rcfrom'))

def fetch_allpages(namespace):
    """
    This fetches all pages from all namespaces and downloads them.
    """
    if namespace is None:
        namespace = range(15)

    info = namespace_info()

    do_commit = False
    for namespace_id in namespace:
        if options.verbose:
            print "fetching page list from namespace %d (%s)" % (namespace_id, info[namespace_id])
        pages = list_pages(namespace_id)
        if len(pages) > 0:
            do_commit = True
        if info[namespace_id] == 'File':
            for page in pages:
                fetch_file(page.getAttribute('title'))
        else:
            for page in pages:
                fetch_page(page.getAttribute('title'))

    if do_commit:
        # finally, add everything to git
        if not os.path.exists(".git"):
            subprocess.check_call([git_path, "init"])
        subprocess.check_call([git_path, "add", "."])
        subprocess.check_call([git_path, "commit", "-m", "automatic import from MediaWiki at url %s" % options.base_url])

def list_pages(namespace_id, apfrom=None):
    """
    Fetch the page list from the XML API
    """
    args = {'apnamespace': namespace_id, 'action': 'query', 'list': 'allpages', 'format': 'xml', 'aplimit': options.limit}
    if apfrom is not None:
        args['apfrom'] = apfrom
    url = "%s/api.php?%s" % (options.base_url, urllib.urlencode(args))
    dom = parseString(urllib.urlopen(url).read())
    pages = dom.getElementsByTagName("p")
    cont = dom.getElementsByTagName('query-continue')
    if options.verbose:
        print "found %d pages" % len(pages)
    if len(cont) > 0:
        pages += list_pages(namespace_id, cont[0].firstChild.getAttribute('apfrom'))
    return pages

def list_cat_pages(namespace_id, category, cmfrom=None):
    global seen_category
    args = {'action': 'query', 'list': 'categorymembers', 'cmtitle': category, 'format': 'xml', 'cmlimit': options.limit}
    if cmfrom is not None:
        args['cmcontinue'] = cmfrom
    if namespace_id:
        args['cmnamespace'] = "|".join(namespace_id)
    url = "%s/api.php?%s" % (options.base_url, urllib.urlencode(args))
    if options.verbose:
        print "fetching page list from category %s and namespace %s in url %s" % (category, namespace_id, url)
    dom = parseString(urllib.urlopen(url).read())
    pages = dom.getElementsByTagName("cm")
    cont = dom.getElementsByTagName('query-continue')
    pages = map(lambda p: p.getAttribute('title'), pages)
    if len(cont) > 0:
        pages += list_cat_pages(namespace_id, category, cont[0].firstChild.getAttribute('cmcontinue').encode('utf-8'))
    if options.recurse:
        seen_category[category] = True
        for page in pages:
            if page.startswith('Category:'):
                if not page in seen_category:
                    pages += list_cat_pages(namespace_id, page.encode('utf-8'))
    if options.verbose:
        print "found %d pages" % len(pages)
    return list(set(pages))
    
def fetch_category(namespace_id, category):
    pages = list_cat_pages(namespace_id, category)
    for page in pages:
        do_commit = True
        if page.startswith('File:'):
            fetch_file(page)
        else:
            fetch_page(page)
    if do_commit:
        # finally, add everything to git
        if not os.path.exists(".git"):
            subprocess.check_call([git_path, "init"])
        subprocess.check_call([git_path, "add", "."])
        subprocess.check_call([git_path, "commit", "-m", "automatic import from MediaWiki at url %s" % options.base_url])

def namespace_info():
    params = urllib.urlencode({'action': 'query', 'meta': 'siteinfo', 'siprop': 'namespaces', 'format': 'xml'})
    url = "%s/api.php?%s" % (options.base_url, params)
    content = urllib.urlopen(url).read()
    dom = parseString(content)
    nss = dom.getElementsByTagName('ns')
    info = {}
    for ns in nss:
        info[int(ns.getAttribute('id'))] = ns.getAttribute('canonical')
    return info

def fetch_page(page, revision = None):
    """
    Fetch the raw text of the page and write it into a .mediawiki file.
    """
    filename = re.sub(r'(^/*)|[\?]|(/../)', '', page).replace(' ', '_')
    if options.convert:
        filename += '.mdwn'
    else:
        filename += ".mediawiki"
    if options.keep and os.path.exists(filename):
        if options.verbose:
            print "skipping already existing page %s" % page
        return None
    if options.verbose:
        print "fetching page %s" % page,
    args = {'title': page.encode('utf-8'), 'action': 'raw'}
    if revision is not None:
        args['oldid'] = revision
    params = urllib.urlencode(args)
    url = options.base_url + "/index.php?%s" % params
    if options.verbose:
        print " from %s into %s" % (url, filename)
    dirname = os.path.dirname(filename)
    # make sure we create a subdir for subpages
    if dirname and not os.path.exists(dirname):
        os.makedirs(dirname)
    if options.convert:
        f = open(filename, 'w')
        f.write(options.convert(urllib.urlopen(url).read()))
        f.close()
    else:
        urllib.urlretrieve(url, filename)
    return filename

def fetch_file(page, revision = None):
    """
    Fetch a file or image.
    """
    filename = re.sub(r'(^/*)|[\?]|(/../)', '', page.replace('File:', '', 1)).replace(' ', '_')
    if options.keep and os.path.exists(filename):
        if options.verbose:
            print "skipping already existing page %s" % page
        return None
    if options.verbose:
        print "fetching file %s locations" % page,
    # api.php?action=query&prop=imageinfo&titles=Image:Cover_The_Last_Hero.jpg&iiprop=url
    args = {'titles': page.encode('utf-8'), 'action': 'query', 'prop': 'imageinfo', 'iiprop': 'url', 'format': 'xml'}
    if revision is not None:
        args['revids'] = revision
    params = urllib.urlencode(args)
    url = "%s/api.php?%s" % (options.base_url, params)
    if options.verbose:
        print " from %s" % (url)
    content = urllib.urlopen(url).read()
    dom = parseString(content)
    url = dom.getElementsByTagName("ii")[0].getAttribute('url')
    if options.verbose:
        print "fetching file %s from %s into %s" % (page, url, filename)
    dirname = os.path.dirname(filename)
    # make sure we create a subdir for subpages
    if dirname and not os.path.exists(dirname):
        os.makedirs(dirname)
    urllib.urlretrieve(url, filename)
    return filename

git_path = "/usr/bin/git"

class options:
    verbose = False
    incremental = False
    category = False
    allpages = False
    namespace = None
    recurse = False
    keep = False
    convert = False
    strict = False
    debugger = False
    base_url = None

def main():
    global options
    usage = "Usage: %prog [ -n <id> ] [ -k ] [ -i | -a | -c <category> [ -r ] ] <url>"
    description = """Convert a Mediawiki site to a git dump suitable for ikiwiki. This will optionnally convert the mediawiki markup to markdown using the MediawikiConverter available at <https://github.com/mithro/media2iki>."""
    epilog = "<url> is the base url of the Mediawiki site, without a trailing index.php (e.g. <http://en.wikipedia.org/w>)."
    parser = OptionParser(usage=usage, description=description, epilog=epilog)
    parser.add_option(
        "-n", "--namespace", type="int",
        help="download only from NAMESPACE",)
    parser.add_option(
        "-v", "--verbose", action="store_true",
        help="be verbose about what we're doing",)
    parser.add_option(
        "-k", "--keep", action="store_true",
        help="keep existing files instead of overwriting",)
    parser.add_option(
        "-t", "--transform", dest="convert", action="store_true",
        help="convert mediawiki markup to markdown",)
    parser.add_option(
        "-i", "--incremental", action="store_true",
        help="incrementally fetch recent changes",)
    parser.add_option(
        "-a", "--all", dest="allpages", action="store_true",
        help="download all pages [default]",)
    parser.add_option(
        "-c", "--category", type="string",
        help="download subpages of the given category",)
    parser.add_option(
        "-r", "--recurse", action="store_true",
        help="recurse into subcategories",)
    parser.add_option(
        "-s", "--strict", action="store_true",
        help="Error on known tags, style or other problems.")
    parser.add_option(
        "-d", "--debugger", action="store_true",
        help="Drop to Python PDB debugger on an error.")
    parser.add_option(
        '-l', '--limit', type='int', default=500,
        help='Limit to the number of objects returned by the API')

    (options, args) = parser.parse_args()
    if len(args) != 1:
        parser.error("wrong number of arguments")

    if options.allpages and options.incremental:
        parser.error("cannot download both all pages and incrementally")
        sys.exit(2)

    options.base_url = args[0].rstrip('/')

    class MediawikiURLopener(urllib.FancyURLopener):
        version = "MediawikiGitDump/1.0 (+http://anarcat.ath.cx/software/mediawikidump.git)"

    urllib._urlopener = MediawikiURLopener()

    sys.path.append(os.path.dirname(os.path.realpath( __file__ )) + '/media2iki')

    if options.convert:
        try:
            import mediawiki2markdown
            mediawiki2markdown.options.STRICT = options.strict
            mediawiki2markdown.options.DEBUGGER = options.debugger
            options.convert = lambda data: mediawiki2markdown.MarkdownConverter().parse(data)
        except Exception, e:
            warn("couldn't load markdown converter: %s - looked into: %s" % (e, sys.path))
            options.convert = False

    if options.incremental:
        fetch_recentchanges(options.namespace)
    elif options.category:
        fetch_category(options.namespace, options.category)
    else:
        fetch_allpages(options.namespace)

if __name__ == "__main__":
  main()
